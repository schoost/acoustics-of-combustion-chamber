function CreateSparseGif(T,X,Y,SkippedFrames,dimX,dimY,indexMap,nodeInfo)

figure
filename = 'evolution_gif.gif';

set(gca, 'FontSize', 18)
set(gcf,'position', [34   164   560   420]);
set(gcf,'paperUnits','centimeters','paperPosition',[0 0 10 8],'paperSize',[10 8])
for i = 1:SkippedFrames:length(T(1,:))
    
    hold off
    T_plot = reshape(mapToFull(T(:,i),indexMap,dimX*dimY),[dimY,dimX]);
    T_plot(find(nodeInfo<0))=NaN;
    surf(X,Y,T_plot);
    xlabel('X');
    ylabel('Y');
    title('Time evolution of cooling fin.');
    %campos([58.6464  -30  130.2245]);
    zlim([ min([min(T_plot), -0.00000001])  max([0.00000001, max(T_plot)])   ])
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    drawnow
    
    frame = getframe (gcf);
    im=frame2im(frame);
    [imind,cm] = rgb2ind(im,8);
    if i==1
        imwrite(imind,cm,filename,'gif','loopcount',inf);
    else
        imwrite(imind,cm,filename,'gif','Writemode','append','Delaytime',0);
    end
    i
    
end