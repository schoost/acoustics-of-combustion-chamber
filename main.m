clear all;
close all

%% INIT
% Define dimension of the shape
h1 = 0.2;
h2 = 0.09;
l1=0.17;
l2=0.736; %max,min = 0.336
c1=0.08;
c2=0.18;

% Number of degrees of freedom (number of nodes per length)
dimX=100;
dimY=100;
ndof = dimX*dimY;

% % Parameter for Robin BC
% alpha = 2;
% Tinf = 90;

% Boundary conditions 
    %0 == dirichlet with in second column the pressure (in bar)
    %1 == Neumann with in second column the pressure flux (in bar)
    %2 == Robin (only defined for West) second column Tinf, third column
    %alpha
    %every row= boundary type: row1=west boundary         row9=Northwestcorner
    %                          row2=east boundary         row10=Northeastcorner
    %                          row3=north boundary        row11=Southwestcorner
    %                          row4=south boundary        row12=Southeastcorner
    %                          row5=east_inner boundary   row13=INW
    %                          row6=north_inner boundary  row14=INE
    %                          row7=west_inner boundary   row15=ISW
    %                          row8=south_inner boundary  row16=ISE
    
boundParameters = [1 0 0;
                    2 0 10^10;
                    1 0 0;
                    1 0 0;
                    1 0 0;
                    1 0 0;
                    1 0 0;
                    1 0 0;
                    1 0 0;
                    1 0 0;
                    1 0 0;
                    1 0 0];
%                     1 0 0;
%                     1 0 0;
%                     1 0 0;
%                     1 0 0];

source = 'oscillating';

%%Scheme...Steady for now

%% Main

%set up mesh
[X,Y,C,nodeInfo,boundOrientation] = SetUpMesh(dimX,dimY,h1,h2,l1,l2,c1,c2);
indexMap = getIndexMap(nodeInfo);

figure(1);
spy(nodeInfo);

%Create sound speed matrix


% Fill matrix A and vector B. Solve the linear system
[A,B] = createSystemMatrices(dimX,dimY,X,Y,nodeInfo,boundOrientation,boundParameters,C);
figure(2);
spy(A);
figure(3);
spy(B);

% Create dense matrices including only the nodes on the domain.
dA = mapToDense(A,indexMap);
dB = mapToDense(B(:),indexMap);

% Create eigenvectors V and eigenvalues D
[V,D] = eigs(dA,10,-0.01);

    
%make some plots

for i = 1:6
    VFull = mapToFull(V(:,i),indexMap,ndof);
    VFull = reshape(VFull,[dimY,dimX]);
    VFull(find(nodeInfo<0))=NaN;
    subplot(3,2,i);
    surf(X,Y,VFull);
    title(D(i,i));
end

figure;
title('Speed of sound over the domain');
C(find(nodeInfo<0))=NaN;
surf(X,Y,C);

%%

N = 50; %Number of eigenvectors considered

[Vn,Dn] = eigs(dA,N,-0.01);
eta0 = zeros(2*N,1);

switch source
    case 'impulse'
        sourcefun = @impulseSource;
    case 'oscillating'
        sourcefun = OscillatingSource(Vn,N,dimY,dimX,indexMap,nodeInfo,25000);
    otherwise
        printf('No source defined');
end
[t,eta]=ODEsolver(Vn,Dn,eta0,N,[0:0.00001:0.01],'ode45',sourcefun,dimX,dimY,indexMap,nodeInfo);
 

%%

P  = [];
for i = 1:length(t)
    P = [P,Vn*eta(i,1:N)'];
end


CreateSparseGif(P,X,Y,1,dimX,dimY,indexMap,nodeInfo);