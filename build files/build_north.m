

% Nomenclature:
%
%    W=(i,j-1) --  w - -P=(i,j) - - e - - E=(i,j+1)
%                  |                |
%   sigmaW    sigmaw    sigma    sigmae    sigmaE
%                  |                |
%      sW - - - -  sw----- s ------se - - - sE
%
%                  |                |
%
%   SW=(i+1,j-1)   Sw  S=(i+1,j) - Se     SE=(i+1,j+1)
%
% Indexing of stecil: 

%    D_3 - D_0 - D3
%     |     |     | 
%    D_2 - D_1 - D4
% Stecil 

% East 
D3=(1/4*dy_Se_e/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_Se_e/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_s_sE+.750000000000000000*dy_sE_E+.500000000000000000*dy_E_P)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.750000000000000000*dx_sE_E-.500000000000000000*dx_E_P)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2)/S_sigma; 

% West 
D_3=(1/4*dy_w_Sw/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_w_Sw/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_sW_s+.500000000000000000*dy_P_W+.750000000000000000*dy_W_sW)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.500000000000000000*dx_P_W-.750000000000000000*dx_W_sW)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigma; 

% South 
D1=((dy_Sw_Se+1/4*dy_Se_e+1/4*dy_w_Sw)/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2-(-dx_Sw_Se-1/4*dx_Se_e-1/4*dx_w_Sw)/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_s_sE+.250000000000000000*dy_P_s)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.250000000000000000*dx_P_s)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2+(1/4*dy_sW_s+.250000000000000000*dy_s_P)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.250000000000000000*dx_s_P)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigma; 

% North 
D_1=0; 

% NW 
D_4=0; 

% NE 
D2=0; 

% SW 
D_2=(1/4*dy_w_Sw/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_w_Sw/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_sW_s+.250000000000000000*dy_W_sW)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.250000000000000000*dx_W_sW)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigma; 

% SE 
D4=(1/4*dy_Se_e/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_Se_e/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_s_sE+.250000000000000000*dy_sE_E)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.250000000000000000*dx_sE_E)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2)/S_sigma; 

% P 
D0=((1/4*dy_Se_e+dy_e_w+1/4*dy_w_Sw)/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2-(-1/4*dx_Se_e-dx_e_w-1/4*dx_w_Sw)/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_s_sE+.500000000000000000*dy_E_P+.750000000000000000*dy_P_s)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.500000000000000000*dx_E_P-.750000000000000000*dx_P_s)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2+(1/4*dy_sW_s+.750000000000000000*dy_s_P+.500000000000000000*dy_P_W)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.750000000000000000*dx_s_P-.500000000000000000*dx_P_W)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigma; 

