

% Nomenclature:
%
%    NW=(i-1,j-1)  Nw - N=(i-1,j)- Ne    NE=(i-1,j+1)
% 
%                  |                |
% 
%       nW- - - -  nw----- n ------ne - - - nE
%                  |                |
%      etaW        etaw   eta    etae      etaE
%                  |                |
%    W=(i,j-1) - - w - - P=(i,j) - -e - - E=(i,j+1)
%
%   SW(i+1,j-1)   Sw  -  S(i+1,j)  - Se      SE(i+1,j+1)
%
% Indexing of stecil: 

%    D_4 - D_1 - D2
%     |     |     | 
%    D_3 - D_0 - D3
% Stecil 

% East 
D3=((.500000000000000000*dy_P_E+.750000000000000000*dy_E_nE+1/4*dy_nE_n)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-.500000000000000000*dx_P_E-.750000000000000000*dx_E_nE-1/4*dx_nE_n)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+(.250000000000000000*dy_P_e+1/4*dy_e_Ne)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-.250000000000000000*dx_P_e-1/4*dx_e_Ne)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_etaepsilon; 

% West 
D_3=0; 

% South 
D1=0; 

% North 
D_1=((1/4*dy_nE_n+.250000000000000000*dy_n_P)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-1/4*dx_nE_n-.250000000000000000*dx_n_P)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+(1/4*dy_e_Ne+.750000000000000000*dy_Ne_N+.500000000000000000*dy_N_P)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-1/4*dx_e_Ne-.750000000000000000*dx_Ne_N-.500000000000000000*dx_N_P)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_etaepsilon; 

% NW 
D_4=0; 

% NE 
D2=((.250000000000000000*dy_E_nE+1/4*dy_nE_n)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-.250000000000000000*dx_E_nE-1/4*dx_nE_n)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+(1/4*dy_e_Ne+.250000000000000000*dy_Ne_N)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-1/4*dx_e_Ne-.250000000000000000*dx_Ne_N)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_etaepsilon; 

% SW 
D_2=0; 

% SE 
D4=0; 

% P 
D0=((.500000000000000000*dy_P_E+1/4*dy_nE_n+.750000000000000000*dy_n_P)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-.500000000000000000*dx_P_E-1/4*dx_nE_n-.750000000000000000*dx_n_P)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+(.750000000000000000*dy_P_e+1/4*dy_e_Ne+.500000000000000000*dy_N_P)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-.750000000000000000*dx_P_e-1/4*dx_e_Ne-.500000000000000000*dx_N_P)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_etaepsilon; 

