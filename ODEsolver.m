function [ t,eta ] = ODEsolver( Vn,Dn,eta0,N,tInterval,solverType,sourceFunction,dimX,dimY,indexMap,nodeInfo )
%ODESOLVER Summary of this function goes here
%   Detailed explanation goes here

eta = zeros(2*N,1);

switch solverType
    case 'ode45' %Matlabs internal ode45 explicit solver
        sysMat = zeros(length(eta),length(eta));
        sysMat(1:N,N+1:end)=eye(N);
        sysMat(N+1:end,1:N)=Dn;
        
        fun = @(t,y) sysMat*y+sourceFunction(Vn,Dn,N,t,y,dimY,dimX,indexMap,nodeInfo);
        
        [t,eta]  = ode23(fun,tInterval,eta0);
    case 'impl_euler'
        delta_t = (tInterval(2)-tInterval(1));
        eta_deta = zeros(2*N,1);
        for i=1:length(tInterval)
            sysMat = zeros(2*N);
            sysMat(1:N,N+1:end)=eye(N);
            sysMat(N+1:end,1:N)=Dn;
            A_star=eye(2*N)-delta_t*sysMat;
            
%         B_i=B(:,:,i);
%         dB_i = mapToDense(B_i(:),indexMap);
%         b=zeros(2*N,1);
%         b(N+1:end,1)=-V'*dB_i;
        b_star=delta_t*sourceFunction(Vn,Dn,N,tInterval(i),eta_deta(:,end),dimY,dimX,indexMap,nodeInfo)+eta_deta(:,i);
        eta_deta(:,i+1)=A_star\b_star;
        %P=[P V*eta_deta(1:N,i)];%each column is P on different time
        end
        eta = eta_deta';
        t = tInterval;
end
end


