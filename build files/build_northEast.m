

% Nomenclature:
%
%    W=(i,j-1) --  w - -P=(i,j) - - e - - E=(i,j+1)
%                  |                |
%   sigmaW    sigmaw    sigma    sigmae    sigmaE
%                  |                |
%      sW - - - -  sw----- s ------se - - - sE
%
%                  |                |
%
%   SW=(i+1,j-1)   Sw  S=(i+1,j) - Se     SE=(i+1,j+1)
%
% Indexing of stecil: 

%    D_3 - D_0 - D3
%     |     |     | 
%    D_2 - D_1 - D4
% Stecil 

% East 
D3=0; 

% West 
D_3=((.250000000000000000*dy_P_w+1/4*dy_w_Sw)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-.250000000000000000*dx_P_w-1/4*dx_w_Sw)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2+(1/4*dy_sW_s+.500000000000000000*dy_P_W+.750000000000000000*dy_W_sW)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.500000000000000000*dx_P_W-.750000000000000000*dx_W_sW)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigmaomega; 

% South 
D1=((.750000000000000000*dy_Sw_S+.500000000000000000*dy_S_P+1/4*dy_w_Sw)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-.750000000000000000*dx_Sw_S-.500000000000000000*dx_S_P-1/4*dx_w_Sw)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2+(1/4*dy_sW_s+.250000000000000000*dy_s_P)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.250000000000000000*dx_s_P)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigmaomega; 

% North 
D_1=0; 

% NW 
D_4=0; 

% NE 
D2=0; 

% SW 
D_2=((.250000000000000000*dy_Sw_S+1/4*dy_w_Sw)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-.250000000000000000*dx_Sw_S-1/4*dx_w_Sw)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2+(1/4*dy_sW_s+.250000000000000000*dy_W_sW)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.250000000000000000*dx_W_sW)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigmaomega; 

% SE 
D4=0; 

% P 
D0=((.500000000000000000*dy_S_P+.750000000000000000*dy_P_w+1/4*dy_w_Sw)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-.500000000000000000*dx_S_P-.750000000000000000*dx_P_w-1/4*dx_w_Sw)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2+(1/4*dy_sW_s+.750000000000000000*dy_s_P+.500000000000000000*dy_P_W)/S_sigmaw*dy_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2-(-1/4*dx_sW_s-.750000000000000000*dx_s_P-.500000000000000000*dx_P_W)/S_sigmaw*dx_w_sw*(1/8*C_SW+1/8*C_S+3/8*C_P+3/8*C_W)^2)/S_sigmaomega; 

