

% Nomenclature:
%
%    NW=(i-1,j-1)  Nw - N=(i-1,j)- Ne    NE=(i-1,j+1)
% 
%                  |                |
% 
%       nW- - - -  nw----- n ------ne - - - nE
%                  |                |
%      etaW        etaw   eta    etae      etaE
%                  |                |
%    W=(i,j-1) - - w - - P=(i,j) - -e - - E=(i,j+1)
%
%   SW(i+1,j-1)   Sw  -  S(i+1,j)  - Se      SE(i+1,j+1)
%
% Indexing of stecil: 

%    D_4 - D_1 - D2
%     |     |     | 
%    D_3 - D_0 - D3
% Stecil 

% East 
D3=0; 

% West 
D_3=((.250000000000000000*dy_w_P+1/4*dy_Nw_w)/S_nomega*dy_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2-(-.250000000000000000*dx_w_P-1/4*dx_Nw_w)/S_nomega*dx_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2+(.500000000000000000*dy_W_P+1/4*dy_n_nW+.750000000000000000*dy_nW_W)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-.500000000000000000*dx_W_P-1/4*dx_n_nW-.750000000000000000*dx_nW_W)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_etaomega; 

% South 
D1=0; 

% North 
D_1=((.500000000000000000*dy_P_N+.750000000000000000*dy_N_Nw+1/4*dy_Nw_w)/S_nomega*dy_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2-(-.500000000000000000*dx_P_N-.750000000000000000*dx_N_Nw-1/4*dx_Nw_w)/S_nomega*dx_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2+(.250000000000000000*dy_P_n+1/4*dy_n_nW)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-.250000000000000000*dx_P_n-1/4*dx_n_nW)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_etaomega; 

% NW 
D_4=((.250000000000000000*dy_N_Nw+1/4*dy_Nw_w)/S_nomega*dy_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2-(-.250000000000000000*dx_N_Nw-1/4*dx_Nw_w)/S_nomega*dx_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2+(1/4*dy_n_nW+.250000000000000000*dy_nW_W)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-1/4*dx_n_nW-.250000000000000000*dx_nW_W)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_etaomega; 

% NE 
D2=0; 

% SW 
D_2=0; 

% SE 
D4=0; 

% P 
D0=((.750000000000000000*dy_w_P+.500000000000000000*dy_P_N+1/4*dy_Nw_w)/S_nomega*dy_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2-(-.750000000000000000*dx_w_P-.500000000000000000*dx_P_N-1/4*dx_Nw_w)/S_nomega*dx_n_nw*(3/8*C_P+3/8*C_N+1/8*C_W+1/8*C_NW)^2+(.500000000000000000*dy_W_P+.750000000000000000*dy_P_n+1/4*dy_n_nW)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-.500000000000000000*dx_W_P-.750000000000000000*dx_P_n-1/4*dx_n_nW)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_etaomega; 

