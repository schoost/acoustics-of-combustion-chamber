

% Nomenclature:
%
%    NW(i-1,j-1)   Nw -  N(i-1,j) -  Ne     NE(i-1,j+1)
%
%                 |                 |
%
%       nW - - - - nw ------ n ------ ne - - - nE
%                 |                 |
%       |         |        |        |       |
%                 |                 |
%   W(i, j-1) - - w - - P (i,j) - - e - -  E (i,j+1)
%                 |                 |
%       |         |        |        |       |
%                 |                 |
%      sW - - - - sw ------ s ------ se - - - sE
%
%                 |                 |
%
%   SW(i+1,j-1)   Sw  -  S(i+1,j)  - Se      SE(i+1,j+1)
%
% Indexing of stecil: 

%    D_4 - D_1 - D2
%     |     |     | 
%    D_3 - D_0 - D3
%     |     |     | 
%    D_2 -  D1 - D4

% Stecil 

% East 
D3=(1/4*dy_Se_e/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_Se_e/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_s_sE+dy_sE_nE+1/4*dy_nE_n)/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-(-1/4*dx_s_sE-dx_sE_nE-1/4*dx_nE_n)/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+1/4*dy_e_Ne/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_e_Ne/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2)/S_P; 

% West 
D_3=(1/4*dy_w_Sw/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_w_Sw/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dy_Nw_w/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_Nw_w/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+(1/4*dy_sW_s+1/4*dy_n_nW+dy_nW_sW)/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2-(-1/4*dx_sW_s-1/4*dx_n_nW-dx_nW_sW)/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_P; 

% South 
D1=((dy_Sw_Se+1/4*dy_Se_e+1/4*dy_w_Sw)/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2-(-dx_Sw_Se-1/4*dx_Se_e-1/4*dx_w_Sw)/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dy_s_sE/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2+1/4*dx_s_sE/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+1/4*dy_sW_s/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_sW_s/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_P; 

% North 
D_1=(1/4*dy_nE_n/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2+1/4*dx_nE_n/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+(1/4*dy_e_Ne+dy_Ne_Nw+1/4*dy_Nw_w)/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2-(-1/4*dx_e_Ne-dx_Ne_Nw-1/4*dx_Nw_w)/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dy_n_nW/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_n_nW/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_P; 

% NW 
D_4=(1/4*dy_Nw_w/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_Nw_w/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dy_n_nW/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_n_nW/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_P; 

% NE 
D2=(1/4*dy_nE_n/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2+1/4*dx_nE_n/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+1/4*dy_e_Ne/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_e_Ne/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2)/S_P; 

% SW 
D_2=(1/4*dy_w_Sw/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_w_Sw/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dy_sW_s/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_sW_s/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_P; 

% SE 
D4=(1/4*dy_Se_e/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dx_Se_e/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+1/4*dy_s_sE/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2+1/4*dx_s_sE/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2)/S_P; 

% P 
D0=((1/4*dy_Se_e+dy_e_w+1/4*dy_w_Sw)/S_s*dy_sw_se*(1/2*C_P+1/2*C_S)^2-(-1/4*dx_Se_e-dx_e_w-1/4*dx_w_Sw)/S_s*dx_sw_se*(1/2*C_P+1/2*C_S)^2+(1/4*dy_s_sE+1/4*dy_nE_n+dy_n_s)/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-(-1/4*dx_s_sE-1/4*dx_nE_n-dx_n_s)/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+(dy_w_e+1/4*dy_e_Ne+1/4*dy_Nw_w)/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2-(-dx_w_e-1/4*dx_e_Ne-1/4*dx_Nw_w)/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+(1/4*dy_sW_s+dy_s_n+1/4*dy_n_nW)/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2-(-1/4*dx_sW_s-dx_s_n-1/4*dx_n_nW)/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_P; 

