

% Nomenclature:
%
%    NW=(i-1,j-1)  Nw - N=(i-1,j)- Ne    NE=(i-1,j+1)
% 
%                  |                |
% 
%       nW- - - -  nw----- n ------ne - - - nE
%                  |                |
%      etaW        etaw   eta    etae      etaE
%                  |                |
%    W=(i,j-1) - - w - - P=(i,j) - -e - - E=(i,j+1)
%
%   SW(i+1,j-1)   Sw  -  S(i+1,j)  - Se      SE(i+1,j+1)
%
% Indexing of stecil: 

%    D_4 - D_1 - D2
%     |     |     | 
%    D_3 - D_0 - D3
% Stecil 

% East 
D3=((.500000000000000000*dy_P_E+.750000000000000000*dy_E_nE+1/4*dy_nE_n)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-.500000000000000000*dx_P_E-.750000000000000000*dx_E_nE-1/4*dx_nE_n)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+1/4*dy_e_Ne/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_e_Ne/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2)/S_eta; 

% West 
D_3=(1/4*dy_Nw_w/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_Nw_w/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+(.500000000000000000*dy_W_P+1/4*dy_n_nW+.750000000000000000*dy_nW_W)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-.500000000000000000*dx_W_P-1/4*dx_n_nW-.750000000000000000*dx_nW_W)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_eta; 

% South 
D1=0; 

% North 
D_1=((1/4*dy_nE_n+.250000000000000000*dy_n_P)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-1/4*dx_nE_n-.250000000000000000*dx_n_P)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+(1/4*dy_e_Ne+dy_Ne_Nw+1/4*dy_Nw_w)/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2-(-1/4*dx_e_Ne-dx_Ne_Nw-1/4*dx_Nw_w)/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+(.250000000000000000*dy_P_n+1/4*dy_n_nW)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-.250000000000000000*dx_P_n-1/4*dx_n_nW)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_eta; 

% NW 
D_4=(1/4*dy_Nw_w/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_Nw_w/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+(1/4*dy_n_nW+.250000000000000000*dy_nW_W)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-1/4*dx_n_nW-.250000000000000000*dx_nW_W)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_eta; 

% NE 
D2=((.250000000000000000*dy_E_nE+1/4*dy_nE_n)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-.250000000000000000*dx_E_nE-1/4*dx_nE_n)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+1/4*dy_e_Ne/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2+1/4*dx_e_Ne/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2)/S_eta; 

% SW 
D_2=0; 

% SE 
D4=0; 

% P 
D0=((.500000000000000000*dy_P_E+1/4*dy_nE_n+.750000000000000000*dy_n_P)/S_etae*dy_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2-(-.500000000000000000*dx_P_E-1/4*dx_nE_n-.750000000000000000*dx_n_P)/S_etae*dx_e_ne*(3/8*C_P+3/8*C_E+1/8*C_NE+1/8*C_N)^2+(dy_w_e+1/4*dy_e_Ne+1/4*dy_Nw_w)/S_n*dy_ne_nw*(1/2*C_P+1/2*C_N)^2-(-dx_w_e-1/4*dx_e_Ne-1/4*dx_Nw_w)/S_n*dx_ne_nw*(1/2*C_P+1/2*C_N)^2+(.500000000000000000*dy_W_P+.750000000000000000*dy_P_n+1/4*dy_n_nW)/S_etaw*dy_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2-(-.500000000000000000*dx_W_P-.750000000000000000*dx_P_n-1/4*dx_n_nW)/S_etaw*dx_nw_w*(3/8*C_P+3/8*C_W+1/8*C_N+1/8*C_NW)^2)/S_eta; 

