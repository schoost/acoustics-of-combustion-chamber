

% Nomenclature:
%
%     N(i-1,j) -  Ne     NE(i-1,j+1)
%
%       |         |         |
%
%      n ------  ne - - - nE
%                 |                 |
%       |         |        |        |       |
%                 |                 |
%   P (i,j) - -   e - -  E (i,j+1)
%                 |                 |
%       |         |        |        |       |
%                 |                 |
%       s ------ se - - - sE
%
%       |         |        |        |
%
%     S(i+1,j)  - Se      SE(i+1,j+1)
%
% Indexing of stecil: 

%   D_1 - D2 
%     |     | 
%   D_0 - D3
%     |     | 
%    D1 - D4

% Stecil 

% East 
D3=((1/4*dy_Se_e+.250000000000000000*dy_e_P)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-1/4*dx_Se_e-.250000000000000000*dx_e_P)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+(1/4*dy_s_sE+dy_sE_nE+1/4*dy_nE_n)/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-(1/4*dx_s_sE+dx_sE_nE+1/4*dx_nE_n)/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+(.250000000000000000*dy_P_e+1/4*dy_e_Ne)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-.250000000000000000*dx_P_e-1/4*dx_e_Ne)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_epsilon; 

% West 
D_3=0; 

% South 
D1=((.750000000000000000*dy_S_Se+1/4*dy_Se_e+.500000000000000000*dy_P_S)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-.750000000000000000*dx_S_Se-1/4*dx_Se_e-.500000000000000000*dx_P_S)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+1/4*dy_s_sE/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-1/4*dx_s_sE/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2)/S_epsilon; 

% North 
D_1=(1/4*dy_nE_n/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-1/4*dx_nE_n/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+(1/4*dy_e_Ne+.750000000000000000*dy_Ne_N+.500000000000000000*dy_N_P)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-1/4*dx_e_Ne-.750000000000000000*dx_Ne_N-.500000000000000000*dx_N_P)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_epsilon; 

% NW 
D_4=0; 

% NE 
D2=(1/4*dy_nE_n/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-1/4*dx_nE_n/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+(1/4*dy_e_Ne+.250000000000000000*dy_Ne_N)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-1/4*dx_e_Ne-.250000000000000000*dx_Ne_N)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_epsilon; 

% SW 
D_2=0; 

% SE 
D4=((.250000000000000000*dy_S_Se+1/4*dy_Se_e)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-.250000000000000000*dx_S_Se-1/4*dx_Se_e)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+1/4*dy_s_sE/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-1/4*dx_s_sE/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2)/S_epsilon; 

% P 
D0=((1/4*dy_Se_e+.750000000000000000*dy_e_P+.500000000000000000*dy_P_S)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-1/4*dx_Se_e-.750000000000000000*dx_e_P-.500000000000000000*dx_P_S)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+(1/4*dy_s_sE+1/4*dy_nE_n+dy_n_s)/S_e*dy_se_ne*(1/2*C_P+1/2*C_E)^2-(1/4*dx_s_sE+1/4*dx_nE_n+dx_n_s)/S_e*dx_se_ne*(1/2*C_P+1/2*C_E)^2+(.750000000000000000*dy_P_e+1/4*dy_e_Ne+.500000000000000000*dy_N_P)/S_nepsilon*dy_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2-(-.750000000000000000*dx_P_e-1/4*dx_e_Ne-.500000000000000000*dx_N_P)/S_nepsilon*dx_ne_n*(3/8*C_P+3/8*C_N+1/8*C_E+1/8*C_NE)^2)/S_epsilon; 

