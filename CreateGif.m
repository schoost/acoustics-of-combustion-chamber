function CreateSparseGif(T,X,Y,SkippedFrames,dimX,dimY)

figure (1)
filename = 'evolution_gif.gif';

set(gca, 'FontSize', 18)
set(gcf,'position', [34   164   560   420]);
set(gcf,'paperUnits','centimeters','paperPosition',[0 0 10 8],'paperSize',[10 8])
for i = 1:SkippedFrames:length(T(1,:))
    
    hold off
    T_plot = reshape(T(:,i),[dimY,dimX]);
    surf(X,Y,T_plot);
    xlabel('X');
    ylabel('Y');
    title('Time evolution of cooling fin.');
    campos([58.6464  -30  130.2245])
    hold on;
    surf(X,-Y,T_plot);
    zlim([ min([min(T_plot), 80])  max([100, max(T_plot)])   ])
    drawnow
    frame = getframe (gcf);
    im=frame2im(frame);
    [imind,cm] = rgb2ind(im,8);
    if i==1
        imwrite(imind,cm,filename,'gif','loopcount',inf);
    else
        imwrite(imind,cm,filename,'gif','Writemode','append','Delaytime',0);
    end
    i
    
end