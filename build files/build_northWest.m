

% Nomenclature:
%
%    W=(i,j-1) --  w - -P=(i,j) - - e - - E=(i,j+1)
%                  |                |
%   sigmaW    sigmaw    sigma    sigmae    sigmaE
%                  |                |
%      sW - - - -  sw----- s ------se - - - sE
%
%                  |                |
%
%   SW=(i+1,j-1)   Sw  S=(i+1,j) - Se     SE=(i+1,j+1)
%
% Indexing of stecil: 

%    D_3 - D_0 - D3
%     |     |     | 
%    D_2 - D_1 - D4
% Stecil 

% East 
D3=((1/4*dy_Se_e+.250000000000000000*dy_e_P)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-1/4*dx_Se_e-.250000000000000000*dx_e_P)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+(1/4*dy_s_sE+.750000000000000000*dy_sE_E+.500000000000000000*dy_E_P)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.750000000000000000*dx_sE_E-.500000000000000000*dx_E_P)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2)/S_sigmaepsilon; 

% West 
D_3=0; 

% South 
D1=((.750000000000000000*dy_S_Se+1/4*dy_Se_e+.500000000000000000*dy_P_S)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-.750000000000000000*dx_S_Se-1/4*dx_Se_e-.500000000000000000*dx_P_S)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+(1/4*dy_s_sE+.250000000000000000*dy_P_s)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.250000000000000000*dx_P_s)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2)/S_sigmaepsilon; 

% North 
D_1=0; 

% NW 
D_4=0; 

% NE 
D2=0; 

% SW 
D_2=0; 

% SE 
D4=((.250000000000000000*dy_S_Se+1/4*dy_Se_e)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-.250000000000000000*dx_S_Se-1/4*dx_Se_e)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+(1/4*dy_s_sE+.250000000000000000*dy_sE_E)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.250000000000000000*dx_sE_E)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2)/S_sigmaepsilon; 

% P 
D0=((1/4*dy_Se_e+.750000000000000000*dy_e_P+.500000000000000000*dy_P_S)/S_sepsilon*dy_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2-(-1/4*dx_Se_e-.750000000000000000*dx_e_P-.500000000000000000*dx_P_S)/S_sepsilon*dx_s_se*(3/8*C_P+3/8*C_S+1/8*C_SE+1/8*C_E)^2+(1/4*dy_s_sE+.500000000000000000*dy_E_P+.750000000000000000*dy_P_s)/S_sigmae*dy_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2-(-1/4*dx_s_sE-.500000000000000000*dx_E_P-.750000000000000000*dx_P_s)/S_sigmae*dx_se_e*(1/8*C_S+1/8*C_SE+3/8*C_E+3/8*C_P)^2)/S_sigmaepsilon; 

