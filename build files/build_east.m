

% Nomenclature:
%
%   NW=(i-1,j-1)    Nw -Nomega - N=(i-1,j)
%
%                   |            |
%
%       nW   - - - -nw- nomega - n
%                   |
%       |           |            |
%                   |
%    W=(i,j-1) - -  w - omega   P=(i,j)
%                   |
%       |           |            |
%                   |
%       sW - - -    sw- somega - s
%
%                   |            |
%
%    SW=(i+1,j-1)   Sw -Somega S=(i+1,j)
%
% Indexing of stecil: 

%    D_4 - D_1 
%     |     |     | 
%    D_3 - D_0 
%     |     |     | 
%    D_2 -  D1 

% Stecil 

% East 
D3=0; 

% West 
D_3=((1/4*dy_Nw_w+.250000000000000000*dy_w_P)/S_nomega*dy_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2-(-1/4*dx_Nw_w-.250000000000000000*dx_w_P)/S_nomega*dx_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2+(1/4*dy_n_nW+dy_nW_sW+1/4*dy_sW_s)/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2-(-1/4*dx_n_nW-dx_nW_sW-1/4*dx_sW_s)/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2+(.250000000000000000*dy_P_w+1/4*dy_w_Sw)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-.250000000000000000*dx_P_w-1/4*dx_w_Sw)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2)/S_omega; 

% South 
D1=(1/4*dy_sW_s/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_sW_s/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2+(.500000000000000000*dy_S_P+1/4*dy_w_Sw+.750000000000000000*dy_Sw_S)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-.500000000000000000*dx_S_P-1/4*dx_w_Sw-.750000000000000000*dx_Sw_S)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2)/S_omega; 

% North 
D_1=((.500000000000000000*dy_P_N+.750000000000000000*dy_N_Nw+1/4*dy_Nw_w)/S_nomega*dy_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2-(-.500000000000000000*dx_P_N-.750000000000000000*dx_N_Nw-1/4*dx_Nw_w)/S_nomega*dx_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2+1/4*dy_n_nW/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_n_nW/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_omega; 

% NW 
D_4=((.250000000000000000*dy_N_Nw+1/4*dy_Nw_w)/S_nomega*dy_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2-(-.250000000000000000*dx_N_Nw-1/4*dx_Nw_w)/S_nomega*dx_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2+1/4*dy_n_nW/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_n_nW/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2)/S_omega; 

% NE 
D2=0; 

% SW 
D_2=(1/4*dy_sW_s/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2+1/4*dx_sW_s/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2+(1/4*dy_w_Sw+.250000000000000000*dy_Sw_S)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-1/4*dx_w_Sw-.250000000000000000*dx_Sw_S)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2)/S_omega; 

% SE 
D4=0; 

% P 
D0=((.500000000000000000*dy_P_N+1/4*dy_Nw_w+.750000000000000000*dy_w_P)/S_nomega*dy_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2-(-.500000000000000000*dx_P_N-1/4*dx_Nw_w-.750000000000000000*dx_w_P)/S_nomega*dx_n_nw*(3/8*C_P+1/8*C_W+3/8*C_N+1/8*C_NW)^2+(dy_s_n+1/4*dy_n_nW+1/4*dy_sW_s)/S_w*dy_nw_sw*(1/2*C_P+1/2*C_W)^2-(-dx_s_n-1/4*dx_n_nW-1/4*dx_sW_s)/S_w*dx_nw_sw*(1/2*C_P+1/2*C_W)^2+(.500000000000000000*dy_S_P+.750000000000000000*dy_P_w+1/4*dy_w_Sw)/S_somega*dy_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2-(-.500000000000000000*dx_S_P-.750000000000000000*dx_P_w-1/4*dx_w_Sw)/S_somega*dx_sw_s*(1/8*C_SW+3/8*C_S+3/8*C_P+1/8*C_W)^2)/S_omega; 

